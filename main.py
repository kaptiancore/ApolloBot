import discord
import logging
import asyncio
from discord.ext import commands
import traceback
import datetime
import time
from cogs.utils import context
import aiohttp
from cogs.utils.db import Table
from cogs.utils.utils import Utils

from creds import token, postgres_creds

start_time = time.time()

# REPO_PATH = os.path.join(os.getcwd())  # git doesnt work with heroku


owner_ids = [320420748680364032, 274287350052552704, 230214242618441728]
bot = commands.Bot(
    command_prefix=('$$'),
    description='Just a bot I made',
    owner_id=320420748680364032,
    case_insensitive=True
)

bot.session = aiohttp.ClientSession(loop=bot.loop)

# bot.repo = git.Repo(REPO_PATH) # note above

initial_extensions = [
    "cogs.math",
    "cogs.mod",
    "cogs.developer",
    "jishaku",
    "cogs.utility",
    "cogs.info",
    "cogs.fun",
    "cogs.bot_server"
]
bot.remove_command("help")

for e in initial_extensions:
    try:
        bot.load_extension(e)
    except Exception as er:
        exc = ''.join(traceback.format_exception(type(er), er, er.__traceback__, chain=False))
        print(exc)
        print(f'Failed to load extension {e}: {er}.')

bot.initial_extensions = initial_extensions

# A task to clean edits to free memory
async def _sweeper():
    while True:
        bot.edits = {}
        await asyncio.sleep(60 * 60)  # 1 Hour


# A task to clean up the bots last bulk deletes
async def _sweep_bulk_deletes():
    while True:
        bot.bulkDeletes = {}
        await asyncio.sleep(60)  # 1 Minute


async def cleanup_code(content):
    # remove ```py\n```
    if content.startswith('```') and content.endswith('```'):
        return '\n'.join(content.split('\n')[1:-1])

    return content.strip('` \n')


async def on_resumed():
    print('resumed...')


async def process_commands(message):
    ctx = await bot.get_context(message, cls=context.Context)

    if ctx.command is None:
        return

    async with ctx.acquire():
        await bot.invoke(ctx)


async def close():
    bot.close()
    await bot.session.close()


@bot.event
async def on_ready():
    bot.loop.create_task(_sweeper())
    bot.loop.create_task(_sweep_bulk_deletes())
    print('Bot is online, and ready to ROLL!')
    while True:
        await bot.change_presence(activity=discord.Game(name=f"with {len(bot.guilds)} servers"))
        await asyncio.sleep(30)
        await bot.change_presence(activity=discord.Game(name="$$help!"))
        await asyncio.sleep(30)
        await bot.change_presence(activity=discord.Game(name="in v2.0, BETA."))
        await asyncio.sleep(30)
        current_time = time.time()
        difference = int(round((current_time - start_time)))
        await bot.change_presence(
            activity=discord.Game(name="online for: " + str(datetime.timedelta(seconds=difference))))
        await asyncio.sleep(30)


@bot.event
async def on_message(message):
    if message.author.id in owner_ids:
        bot.owner_id = message.author.id
    if message.author.bot:
        return

    await process_commands(message)


@bot.command()
async def say(ctx, *, message: commands.clean_content()):
    '''I say what you want me to say. Oh boi...'''
    try:
        await ctx.message.delete()
    except discord.Forbidden:
        pass
    finally:
        await ctx.send(message)

loop = asyncio.get_event_loop()

pool = loop.run_until_complete(Table.create_pool(postgres_creds, command_timeout=60))
bot.pool = pool

bot.run(token, reconnect=True)
