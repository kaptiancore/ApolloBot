import os
import discord
import json_store_client
from discord.ext import commands


class Economy(commands.Cog):
    def __init__(self, bot): 
        self.bot = bot 
        self.db = json_store_client.Client(os.getenv("database_url"))
    

    @commands.command()
    @commands.cooldown(1, 86400)
    async def daily(self, ctx):
        """daily cash injection"""
        self.db.store(f"users/{ctx.author.id}/money/wallet", int(self.db.get(f"users/{ctx.author.id}/money/wallet"))+100)
        await self.bot.send("Your money", "100 coins has been added to your balance")
    

    @commands.command()
    @commands.cooldown(2, 3)
    async def bal(self, ctx):
        """checks your current balance"""
        bal = self.db.get(f"users/{ctx.author.id}/money/wallet")
        await ctx.send(f"Your balance is **{bal}**")
    

def setup(bot): bot.add_cog(Economy(bot))
