import discord
import datetime
import time
from discord.ext import commands


class Statistics(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command(self, ctx):
        query = "INSERT INTO commands (guild_id, channel_id, author_id, used, prefix, command)" \
                " VALUES ($1, $2, $3, $4, $5, $6)"

        if ctx.guild is None:
            guild_id = None
        else:
            guild_id = ctx.guild.id

        await self.bot.pool.execute(query, guild_id,
                                    ctx.channel.id, ctx.author.id,
                                    ctx.message.created_at, ctx.prefix,
                                    ctx.command.qualified_name)

    @commands.command()
    async def stats(self, ctx):
        """Statsies for this bot. Be a nerd!""" 
        member = 0
        for i in self.bot.guilds:
            member += len(i.members)

        query = "SELECT COUNT(*), MIN(used) FROM commands WHERE guild_id=$1;"
        count = await ctx.db.fetchrow(query, ctx.guild.id)

        current_time = datetime.datetime.utcnow()
        difference = int(round((current_time - ctx.bot.uptime).total_seconds()))
        color = discord.Color(value=0x00ff00)
        em = discord.Embed(color=color, title='Bot Stats')
        em.description = "These are some stats for Apollo Bot#1064."
        em.set_thumbnail(url="https://cdn.discordapp.com/avatars/508444259280617482/3aee63cd0ff0de7519627799fa6bf44a.webp?size=1024")        
        em.add_field(name='Creators', value='Nonna#6534 & Kaptian Core#5274')
        em.add_field(name='Devs', value='mathsman#1208')
        em.add_field(name='Servers :homes: ', value=f'{len(self.bot.guilds)} servers')
        em.add_field(name='Total Members :busts_in_silhouette: ', value=member)
        em.add_field(name='Version :new:', value='2.0 BETA')
        em.add_field(name='Start Date :calendar:', value='01/01/2019')
        em.add_field(name='Uptime :stopwatch:', value=str(datetime.timedelta(seconds=difference)))
        em.add_field(name='Bot Region :earth_asia:', value='Australia')
        em.add_field(name='Latency :ping_pong: ', value=f"{self.bot.latency * 1000:.4f} ms")
        em.add_field(name='Hosting Platform :desktop:', value='Self Hosted')
        em.add_field(name='Coding Language :computer: ', value='Python, discord.py (rewrite)')
        em.add_field(name='Total Commands Used', value=f'{count[0]} commands')
        em.set_footer(text='Tracking Commands Since').timestamp = count[1] or datetime.datetime.utcnow()
        await ctx.send(embed=em)

    @commands.command(aliases=['si', 'server'])
    async def serverinfo(self, ctx):
        '''Get server info'''
        guild = ctx.guild
        guild_age = (ctx.message.created_at - guild.created_at).days
        created_at = f"Server created on {guild.created_at.strftime('%b %d %Y at %H:%M')}. That\'s over {guild_age} days ago!"
        color = discord.Color.green()

        em = discord.Embed(description=created_at, color=color)
        em.add_field(name='Online Members', value=len({m.id for m in guild.members if m.status is not discord.Status.offline}))
        em.add_field(name='Total Members', value=len(guild.members))
        em.add_field(name='Text Channels', value=len(guild.text_channels))
        em.add_field(name='Voice Channels', value=len(guild.voice_channels))
        em.add_field(name='Roles', value=len(guild.roles))
        em.add_field(name='Owner', value=guild.owner)

        em.set_thumbnail(url=None or guild.icon_url)
        em.set_author(name=guild.name, icon_url=None or guild.icon_url)
        await ctx.send(embed=em)

    @commands.command()
    async def userinfo(self, ctx, user: discord.Member=None):
        """Dig out that user info. Usage: *userinfo [tag user]"""
        if not user:
            user = ctx.message.author

        color = discord.Color(value=0xf2f760)
        em = discord.Embed(color=color, title=f'User Info: {user.name}')
        em.add_field(name='Status', value=f'{user.status}')
        em.add_field(name='Account Created', value=user.created_at.__format__('%A, %B %d, %Y'))
        em.add_field(name='ID', value=f'{user.id}')
        em.set_thumbnail(url=user.avatar_url)
        await ctx.send(embed=em)

    @commands.command(aliases=['role'])
    async def roleinfo(self, ctx, *, rolename):
        '''Get information about a role. Case Sensitive!'''
        try:
            role = discord.utils.get(ctx.message.guild.roles, name=rolename)
        except:
            return await ctx.send(f"Role could not be found. The system IS case sensitive!")

        em = discord.Embed(description=f'Role ID: {str(role.id)}', color=role.color or discord.Color.green())
        em.title = role.name
        perms = ""
        if role.permissions.administrator:
            perms += "Administrator, "
        if role.permissions.create_instant_invite:
            perms += "Create Instant Invite, "
        if role.permissions.kick_members:
            perms += "Kick Members, "
        if role.permissions.ban_members:
            perms += "Ban Members, "
        if role.permissions.manage_channels:
            perms += "Manage Channels, "
        if role.permissions.manage_guild:
            perms += "Manage Guild, "
        if role.permissions.add_reactions:
            perms += "Add Reactions, "
        if role.permissions.view_audit_log:
            perms += "View Audit Log, "
        if role.permissions.read_messages:
            perms += "Read Messages, "
        if role.permissions.send_messages:
            perms += "Send Messages, "
        if role.permissions.send_tts_messages:
            perms += "Send TTS Messages, "
        if role.permissions.manage_messages:
            perms += "Manage Messages, "
        if role.permissions.embed_links:
            perms += "Embed Links, "
        if role.permissions.attach_files:
            perms += "Attach Files, "
        if role.permissions.read_message_history:
            perms += "Read Message History, "
        if role.permissions.mention_everyone:
            perms += "Mention Everyone, "
        if role.permissions.external_emojis:
            perms += "Use External Emojis, "
        if role.permissions.connect:
            perms += "Connect to Voice, "
        if role.permissions.speak:
            perms += "Speak, "
        if role.permissions.mute_members:
            perms += "Mute Members, "
        if role.permissions.deafen_members:
            perms += "Deafen Members, "
        if role.permissions.move_members:
            perms += "Move Members, "
        if role.permissions.use_voice_activation:
            perms += "Use Voice Activation, "
        if role.permissions.change_nickname:
            perms += "Change Nickname, "
        if role.permissions.manage_nicknames:
            perms += "Manage Nicknames, "
        if role.permissions.manage_roles:
            perms += "Manage Roles, "
        if role.permissions.manage_webhooks:
            perms += "Manage Webhooks, "
        if role.permissions.manage_emojis:
            perms += "Manage Emojis, "

        if perms is None:
            perms = "None"
        else:
            perms = perms.strip(", ")

        em.add_field(name='Hoisted', value=str(role.hoist))
        em.add_field(name='Position from bottom', value=str(role.position))
        em.add_field(name='Managed by Integration', value=str(role.managed))
        em.add_field(name='Mentionable', value=str(role.mentionable))
        em.add_field(name='People in this role', value=str(len(role.members)))

        await ctx.send(embed=em)

        em2 = discord.Embed(description=f'Role ID: {str(role.id)}', color=role.color or discord.Color.green())
        em2.title = role.name
        em2.add_field(name='Permissions', value=perms)

        await ctx.send(embed=em2)
        

def setup(bot):
    if not hasattr(bot, 'uptime'):
        bot.uptime = datetime.datetime.utcnow()

    bot.add_cog(Statistics(bot))
