from discord.ext import commands
import discord
import traceback
import datetime

SERVER = 529590413078822913

USER_ROLE = 540396603295399937
JOIN_LEAVE_CHANNEL = 539372969110274049
COMMAND_LOG_CHANNEL = 530586774288728074
ERROR_LOG_CHANNEL = 539303717237948437


class ServerSpecifics(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_member_join(self, member):
        if member.bot:
            return

        role = member.guild.get_role(USER_ROLE)
        await member.add_roles(role, reason='Auto Role on Join')

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        await self.send_guild_stats(guild, join=True)

    @commands.Cog.listener()
    async def on_guild_remove(self, guild):
        await self.send_guild_stats(guild, join=False)

    @commands.Cog.listener()
    async def on_command(self, ctx):
        if ctx.command.cog_name:
            if ctx.command.cog_name == 'Developer':
                return

        channel = self.bot.get_channel(COMMAND_LOG_CHANNEL)

        e = discord.Embed(color=discord.Colour.blue(), title="Command Run!")
        e.set_author(name=str(ctx.author), icon_url=ctx.author.avatar_url)
        e.add_field(name="User ID", value=ctx.author.id)
        e.add_field(name="Server", value=ctx.guild.name)
        e.add_field(name="Server ID", value=ctx.guild.id)
        e.add_field(name="Channel", value=ctx.channel.name)
        e.add_field(name="Command Content", value=f"```{ctx.message.content}```")
        e.set_thumbnail(url=ctx.guild.icon_url)

        await channel.send(embed=e)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        e = discord.Embed(color=discord.Color.red(), title='An error occurred.')
        missing_param_errors = (commands.MissingRequiredArgument,
                                commands.BadArgument, commands.TooManyArguments, commands.UserInputError)

        ignored = (commands.NoPrivateMessage, commands.DisabledCommand, commands.CheckFailure,
                   commands.CommandNotFound, commands.UserInputError, discord.Forbidden)

        if isinstance(error, ignored):
            return

        if isinstance(error, missing_param_errors):
            e.title = 'Incorrect Usage of Command'
            e.description = f"This is the correct usage:\n**{ctx.prefix}{ctx.command.signature}**"
            return await ctx.send(embed=e)

        if isinstance(error, commands.NotOwner):
            e.description = 'Not my daddy! This command is for the owner only.'
            return await ctx.send(embed=e)

        elif isinstance(error, commands.MissingPermissions):
            missing = "\n".join([self.bot.utils.capitalize(x) for x in error.missing_perms])
            return await ctx.send(
                f"You don't have permission to run this command! Maybe try getting these permissions:\n\n{missing}")

        elif isinstance(error, commands.CommandOnCooldown):
            retry_time = error.retry_after
            if retry_time < 60:
                actual_time = f"{int(retry_time)} seconds"
            elif retry_time >= 60 and retry_time < 3600:
                actual_time = f"{int(retry_time / 60)} minutes"
            elif retry_time >= 3600 and retry_time < 86400:
                actual_time = f"{int(retry_time / 3600)} hours"
            elif retry_time >= 86400:
                actual_time = f"{int(retry_time / 86400)} days"
            return await ctx.send(f"BAKA! You're using this command too fast."
                                  f" Don't make me repeat myself, and wait for **{actual_time}**.")

        e = discord.Embed(colour=discord.Colour.red(), title='Command Error')
        e.description = 'Something unexpected happened. ' \
                        '\n\nIt has been reported to the devs.' \
                        '\n\nPlease join the [support server](https://discord.gg/DTVRTBF)' \
                        ' for more help.'
        e.set_footer(text='Apologies for the inconvenience caused.').timestamp=datetime.datetime.utcnow()
        await ctx.send(embed=e)

        e = discord.Embed(title='Command Error', colour=0xcc3366)
        e.add_field(name='Name', value=ctx.command.qualified_name)
        e.add_field(name='Author', value=f'{ctx.author} (ID: {ctx.author.id})')

        fmt = f'Channel: {ctx.channel} (ID: {ctx.channel.id})'
        if ctx.guild:
            fmt = f'{fmt}\nGuild: {ctx.guild} (ID: {ctx.guild.id})'

        e.add_field(name='Location', value=fmt, inline=False)

        exc = ''.join(traceback.format_exception(type(error), error, error.__traceback__, chain=False))
        e.description = f'```py\n{exc}\n```'
        e.timestamp = datetime.datetime.utcnow()

        await self.bot.get_channel(ERROR_LOG_CHANNEL).send(embed=e)

    async def send_guild_stats(self, guild, join=True):
        channel = guild.get_channel(JOIN_LEAVE_CHANNEL)

        e = discord.Embed(color=discord.Color.green() if join else discord.Colour.red())
        e.title = f"Apollo Bot has {'joined a new' if join else 'been removed from a'} server!"
        e.description = f"**{guild.name}**"
        e.add_field(name="Owner", value=str(guild.owner))
        e.add_field(name="Member Count", value=len(guild.members))
        e.add_field(name="Server Count", value=len(self.bot.guilds))
        e.set_footer(text=f"ID: {guild.id}")
        e.set_thumbnail(url=guild.icon_url)
        await channel.send(embed=e)


def setup(bot):
    bot.add_cog(ServerSpecifics(bot))