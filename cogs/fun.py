import discord
import asyncio
import random
from discord.ext import commands
from .utils.utils import Utils


class Fun(commands.Cog):
    def __init__(self, bot):
       self.bot = bot
       self.utils = Utils(bot)
       self.guess_number = random.randint(1, 100)
       # Blackjack variables
       playingBlackJack = False
       dealerValue = 0
       playerValue = 0
       dealerCards = []
       playerCards = []
       dealerNumAces = 0
       playerNumAces = 0
       cardNames = {1: 'One', 2: 'Two', 3: 'Three', 4: 'Four', 5: 'Five', 6: 'Six', 7: 'Seven', 8: 'Eight', 9: 'Nine', 10: 'Ten', 11: 'Jack', 12: 'Queen', 13: 'King', 14: 'Ace'}
       cardValues = {1: 1, 2: 2, 3: 3, 4: 4, 5: 5, 6: 6, 7: 7, 8: 8, 9: 9, 10: 10, 11: 10, 12: 10, 13: 10, 14: 11}
        
    @commands.command()
    async def blackjack(self, *, ctx):
        await ctx.say("""`
     _     _            _     _            _    
    | |__ | | __ _  ___| | __(_) __ _  ___| | __
    |  _ \| |/ _  |/ __| |/ /| |/ _  |/ __| |/ /
    | |_) | | (_| | (__|   < | | (_| | (__|   < 
    |_ __/|_|\__,_|\___|_|\_\/ |\__ _|\___|_|\_
                       |__/                                                                           
        `""")
        await resetBlackJack(1)
        global playingBlackJack, dealerValue, playerValue, dealerCards, playerCards, dealerNumAces, playerNumAces, cardNames, cardValues
        playingBlackJack = True

        #Simulate dealer's turn
        while dealerValue < 17:
            nextCard = random.randrange(1,15)
            if nextCard is 14: dealerNumAces += 1
            dealerCards.append(cardNames[nextCard])
            dealerValue += cardValues[nextCard]
            while dealerValue > 21:
                if(dealerNumAces > 0):
                    dealerValue -= 10
                    dealerNumAces -= 1
                else: break

        #Give player 2 cards
        for i in range(0,2):
            nextCard = random.randrange(1, 15)
            playerValue += cardValues[nextCard]
            playerCards.append(cardNames[nextCard])

        #print("Dealer value: " + str(dealerValue))
        #print("Dealer cards: " + str.join(" ", dealerCards))
        await ctx.say("Say $hit to be dealt another card, and $stay to stick with your current total value.")
        await ctx.say("Dealer's first card is: " + dealerCards[0])
        await printCards(1) #Print player cards/value

    @commands.command()
    async def hit(self, *, ctx):
        if playingBlackJack is False:
            await ctx.say("Type $blackjack to begin a game of blackjack")
            return
        global playerValue, playerCards, playerNumAces
        nextCard = random.randrange(1, 15)
        if nextCard is 14: playerNumAces += 1
        playerCards.append(cardNames[nextCard])
        playerValue += cardValues[nextCard]
        while playerValue > 21:
            if (playerNumAces > 0):
                playerValue -= 10
                playerNumAces -= 1
            else:
                await resetBlackJack(0)
                break
        await printCards(1) #Print player cards/value

    @commands.command()
    async def stay(self, *, ctx):
        if playingBlackJack is False:
            await ctx.say("Type $blackjack to begin a game of blackjack")
            return
        await resetBlackJack(0) #End the game

    @commands.command()
    async def pop(self, ctx, country: str):
        """get the population of any country"""
        count = country.title()

        async with self.bot.session.request(
                'get', f"http://api.population.io:80/1.0/population/{count}/today-and-tomorrow/") as r:
            data2 = await r.json()

        try:
            popu = data2["total_population"][0]["population"]
            final = f"Today there are {popu} people in {count}"
            await ctx.send(final)
        except:
            raise commands.BadArgument('Country not found!')

    @commands.command(aliases=['joke', 'badjoke', 'hjoke'])
    async def horriblejoke(self, ctx):
        """It's a REALLY REALLY bad joke. Trust me."""
        async with self.bot.session.request('get', "https://geek-jokes.sameerkumar.website/api") as r:
            data2 = await r.json()

        color = discord.Color(value=0x00ff00)
        em = discord.Embed(color=color, title="A really bad joke!")
        em.description = data2
        await ctx.send(embed=em)
 
    @commands.command(aliases=['lf'])
    async def lifehack(self, ctx):
        """Learn a new way to hack life, one at a time."""
        choices = ["If you want to buy the cheapest airline tickets online, use your browser's incognito mode. Prices go up if you visit a site multiple times.", "An iPad charger will charge your iPhone much faster.", "Changing the font size of periods from 12 to 14 makes a paper look significantly longer.", "The harder you concentrate on falling asleep, the harder it is to actually fall asleep.", "Trucks drivers are always communicating with each other on the road. If you see one slow down for no reason, there's probably a cop ahead.", "Restaurant are required to give you free water.", "By peeing in the shower, you can save about 1157 gallons of water a year!", "Lick your wrist and smell it. This is what your breath smells like to others."]
        color = discord.Color(value=0x00ff00)
        em = discord.Embed(color=color, title="Life Hack!")
        em.description = random.choice(choices) 
        em.set_footer(text="Source: Crumblyy")
        await ctx.send(embed=em)
        
    @commands.command()
    async def hack(self, ctx, user: discord.Member):
        """Hack someone's account! Try it!"""
        msg = await ctx.send(f"Hacking! Target: {user}")
        await asyncio.sleep(2)
        await msg.edit(content="Accessing Discord Files... [▓▓    ]")
        await asyncio.sleep(2)
        await msg.edit(content="Accessing Discord Files... [▓▓▓   ]")
        await asyncio.sleep(2)
        await msg.edit(content="Accessing Discord Files... [▓▓▓▓▓ ]")
        await asyncio.sleep(2)
        await msg.edit(content="Accessing Discord Files COMPLETE! [▓▓▓▓▓▓]")
        await asyncio.sleep(2)
        await msg.edit(content="Retrieving Login Info... [▓▓▓    ]")
        await asyncio.sleep(3)
        await msg.edit(content="Retrieving Login Info... [▓▓▓▓▓ ]")
        await asyncio.sleep(3)
        await msg.edit(content="Retrieving Login Info... [▓▓▓▓▓▓ ]")
        await asyncio.sleep(4)
        await msg.edit(content=f"An error has occurred hacking {user}'s account. Please try again later. ❌")   

    @commands.command(aliases=['animation', 'a'])
    async def anim(self, ctx, type):
        """Animations! Usage: *anim [type]. For a list, use *anim list."""
        if type is None:
            await ctx.send('Probably a really cool animation, but we have not added them yet! But hang in there! You never know... For a current list, type *anim list')
        else:
            if type.lower() == 'wtf':
                msg = await ctx.send("```W```")
                await asyncio.sleep(1)
                await msg.edit(content="```WO```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT D```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT DA```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT DA F```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT DA FR```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT DA FRI```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT DA FRIC```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT DA FRICK```")
                await asyncio.sleep(1)
                await msg.edit(content="```WOT DA FRICK!```")
                await asyncio.sleep(1)
                await msg.edit(content="WOT DA FRICK!")
            elif type.lower() == 'mom':
                msg = await ctx.send("```Y```")
                await asyncio.sleep(1)
                await msg.edit(content="```YO```")
                await asyncio.sleep(1)
                await msg.edit(content="```YO M```")
                await asyncio.sleep(1)
                await msg.edit(content="```YO MO```")
                await asyncio.sleep(1)
                await msg.edit(content="```YO MOM```")
                await asyncio.sleep(1)
                await msg.edit(content="```YO MOM!```")
                await asyncio.sleep(1)
                await msg.edit(content="YO MOM!")
            elif type.lower() == 'gethelp':
                msg = await ctx.send("```STOP!```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! G```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! Ge```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! Get```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! Get s```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! Get so```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! Get som```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! Get some```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! Get some HELP```")
                await asyncio.sleep(1)
                await msg.edit(content="```STOP! Get some HELP!!!```")
                await asyncio.sleep(1)
                await msg.edit(content="STOP! Get some HELP!!!")
            elif type.lower() == 'sike':
                msg = await ctx.send("```W```")
                await asyncio.sleep(1)
                await msg.edit(content="```Wa```")
                await asyncio.sleep(1)
                await msg.edit(content="```Wai```")
                await asyncio.sleep(1)
                await msg.edit(content="```Wait```")
                await asyncio.sleep(1)
                await msg.edit(content="```Wait.```")
                await asyncio.sleep(1)
                await msg.edit(content="```Wait..```")
                await asyncio.sleep(1)
                await msg.edit(content="```Wait...```")
                await asyncio.sleep(1)
                await msg.edit(content="```SIKE!```")
                await asyncio.sleep(1)
                await msg.edit(content="SIKE!")
            elif type.lower() == 'gitgud':
                msg = await ctx.send("```G```")
                await asyncio.sleep(1)
                await msg.edit(content="```Gi```")
                await asyncio.sleep(1)
                await msg.edit(content="```Git```")
                await asyncio.sleep(1)
                await msg.edit(content="```Git G```")
                await asyncio.sleep(1)
                await msg.edit(content="```Git GU```")
                await asyncio.sleep(1)
                await msg.edit(content="```Git GUD```")
                await asyncio.sleep(1)
                await msg.edit(content="```Git GUD!```")
                await asyncio.sleep(1)
                await msg.edit(content="Git GUD!")
            elif type.lower() == 'clock':
                msg = await ctx.send(":clock12:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock1230:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock1:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock130:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock2:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock230:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock3:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock330:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock4:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock430:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock5:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock530:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock6:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock630:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock7:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock730:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock8:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock830:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock9:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock930:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock10:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock1030:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock11:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock1130:")
                await asyncio.sleep(1)
                await msg.edit(content=":clock12:")
            elif type.lower() == 'mate':
                msg = await ctx.send("```Y```")
                await asyncio.sleep(1)
                await msg.edit(content="```Ye```")
                await asyncio.sleep(1)
                await msg.edit(content="```Ye W```")
                await asyncio.sleep(1)
                await msg.edit(content="```Ye WO```")
                await asyncio.sleep(1)
                await msg.edit(content="```Ye WOT```")
                await asyncio.sleep(1)
                await msg.edit(content="```Ye WOT M8```")
                await asyncio.sleep(1)
                await msg.edit(content="```Ye WOT M8?!?!?!")
                await asyncio.sleep(1)
                await msg.edit(content="Ye WOT M8?!?!?!")
            elif type.lower() == 'oj':
                msg = await ctx.send("```M```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mm```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mmm```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mmm i```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mmm it```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mmm it'```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mmm it's```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mmm it's a```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mmm it's a ORANGE```")
                await asyncio.sleep(1)
                await msg.edit(content="```Mmm it's a ORANGE JUICE```")
                await asyncio.sleep(1)
                await msg.edit(content="Mmm it's a ORANGE JUICE")
            elif type.lower() == 'list':
                color = discord.Color(value=0x00ff00)
                em = discord.Embed(color=color, title="Current List of Awesome Animations:")
                em.description = "wtf (anim wtf), mom (anim mom), gethelp (anim gethelp), sike (anim sike), gitgud (anim gitgud), clock (anim clock), mate (anim mate), oj (anim oj)."
                em.set_footer(text="We will always be adding new animations!")
                await ctx.send(embed=em)
            else:
                await ctx.send('Probably a really cool animation, but we have not added them yet! But hang in there! You never know... For a current list, type *anim list')

    @commands.command()
    async def insult(self, ctx, user: discord.Member = None):
        '''insult someone! If you suck at insulting them yourself.'''
        msg = f"Hey, {user.mention if user else ctx.author.mention}! "
        roasts = ["I'd give you a nasty look but you've already got one.", "If you're going to be two-faced, at least make one of them pretty.", "The only way you'll ever get laid is if you crawl up a chicken's ass and wait.", "It looks like your face caught fire and someone tried to put it out with a hammer.", "I'd like to see things from your point of view, but I can't seem to get my head that far up your ass.", "Scientists say the universe is made up of neutrons, protons and electrons. They forgot to mention morons.", "Why is it acceptable for you to be an idiot but not for me to point it out?", "Just because you have one doesn't mean you need to act like one.", "Someday you'll go far... and I hope you stay there.", "Which sexual position produces the ugliest children? Ask your mother.", "No, those pants don't make you look fatter - how could they?", "Save your breath - you'll need it to blow up your date.", "If you really want to know about mistakes, you should ask your parents.", "Whatever kind of look you were going for, you missed.", "Hey, you have something on your chin... no, the 3rd one down.", "I don't know what makes you so stupid, but it really works.", "You are proof that evolution can go in reverse.", "Brains aren't everything. In your case they're nothing.", "I thought of you today. It reminded me to take the garbage out.", "You're so ugly when you look in the mirror, your reflection looks away.", "Quick - check your face! I just found your nose in my business.", "It's better to let someone think you're stupid than open your mouth and prove it.", "You're such a beautiful, intelligent, wonderful person. Oh I'm sorry, I thought we were having a lying competition.", "I'd slap you but I don't want to make your face look any better.", "You have the right to remain silent because whatever you say will probably be stupid anyway."]
        await ctx.send(f"{msg} {random.choice(roasts)}")

    @commands.command()
    async def yomomma(self, ctx):
        '''Sends a random yo momma joke. Outdated?'''

        async with self.bot.session.request('get', "http://api.yomomma.info/") as r:
            data = await r.json(content_type='text/html')

        await ctx.send(data['joke'])

    @commands.command(aliases=['8ball'])
    async def eightball(self, ctx, *, message: str):
        """Really desperate? Ask the 8ball for advice. Only yes/no questions!"""
        choices = ['It is certain. :white_check_mark:', 'It is decidedly so. :white_check_mark:', 'Without a doubt. :white_check_mark:', 'Yes, definitely. :white_check_mark:', 'You may rely on it. :white_check_mark:', 'As I see it, yes. :white_check_mark:', 'Most likely. :white_check_mark:', ' Outlook good. :white_check_mark:', 'Yes. :white_check_mark:', 'Signs point to yes. :white_check_mark:', 'Reply hazy, try again. :large_orange_diamond: ', 'Ask again later. :large_orange_diamond: ', 'Better not tell you now. :large_orange_diamond: ', 'Cannot predict now. :large_orange_diamond: ', 'Concentrate and ask again. :large_orange_diamond: ', 'Do not count on it. :x:', 'My reply is no. :x:', 'My sources say no. :x:', 'Outlook not so good. :x:', 'Very doubtful. :x:']
        color = discord.Color(value=0x00ff00)
        em = discord.Embed(color=color, title=f"{message}")
        em.description = random.choice(choices)
        em.set_author(name="The Mighty 8 ball", icon_url="https://vignette.wikia.nocookie.net/battlefordreamislandfanfiction/images/5/53/8-ball_my_friend.png/revision/latest?cb=20161109021012")
        em.set_footer(text=f"Sent by {ctx.message.author.name}")

        if ctx.me.permissions.manage_messages:
            await ctx.message.delete()

        await ctx.send(embed=em)

    @commands.command()
    async def punch(self, ctx, user: discord.Member):
        """Punches the specified user."""
        embed = discord.Embed(title="Now Punching", description=user.mention, color=0x149900)
        await ctx.send(embed=embed)
        await asyncio.sleep(1)
        embed_2 = discord.Embed(title="You've been punched!", description="By " + ctx.message.author.name, color=0x149900)
        await user.send(embed=embed_2)


def setup(bot): 
    bot.add_cog(Fun(bot))

