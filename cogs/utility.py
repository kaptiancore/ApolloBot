import discord
import asyncio
import random
from discord.ext import commands


class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        
        
    @commands.command(pass_context=True)
    async def autorole(self, ctx, *, group: discord.Role = None):
        """Gives yourself a role"""
        if group is None:
            return await self.bot.say("You haven't specified a role! ")
        if group not in ctx.message.server.roles:
            return await self.bot.say("That role doesn't exist.")
        if group not in ctx.message.author.roles:
            await self.bot.add_roles(ctx.message.author, role)
            return await self.bot.say("{} role has been added to {}.".format(role, ctx.message.author.mention))
        if group in ctx.message.author.roles:
            await self.bot.remove_roles(ctx.message.author, role)
            return await self.bot.say("{} role has been removed from {}.".format(role, ctx.message.author.mention))
        
        
    @commands.command()
    async def timer(self, ctx, timer: int):
        """Counts down till it's over! Usage: *timer [time in secs]"""
        await ctx.send("Timer started and counting! :timer:")
        await asyncio.sleep(timer)
        await ctx.send("TIME'S UP! :clock:")
        
    @commands.command()
    async def ranint(self, ctx, a: int, b: int):
        """Usage: *ranint [least number][greatest number]. RanDOM!"""
        if a is None:
            await ctx.send("Boi, are you random! Usage: *ranint [least #] [greatest #], to set the range of the randomized number. Please use integers.")
        if b is None:
            await ctx.send("Boi, are you random! Usage: *ranint [least #] [greatest #], to set the range of the randomized number. Please use integers.")
        else:
            color = discord.Color(value=0x00ff00)
            em = discord.Embed(color=color, title='Your randomized number:')
            em.description = random.randint(a,b)
            await ctx.send(embed=em)

    @commands.command()
    async def rolldice(self, ctx):
        """Rolls a 6 sided die."""
        choices = ['1', '2', '3', '4', '5', '6']
        color = discord.Color(value=0x00ff00)
        em = discord.Embed(color=color, title='Rolled! (1 6-sided die)', description=random.choice(choices))
        await ctx.send(embed=em)

    @commands.command()
    async def flipcoin(self, ctx):
        """Flip a coin. Any coin."""
        choices = ['Heads', 'Tails', 'Coin self-destructed.']
        color = discord.Color(value=0x00ff00)
        em=discord.Embed(color=color, title='Flipped a coin!')
        em.description = random.choice(choices)
        await ctx.send(embed=em)

        
    @commands.command(aliases=['tf'])
    async def textface(self, ctx, Type):
        """Get those dank/cool faces here. Type *textface list for a list."""
        if Type is None:
            await ctx.send('That is NOT one of the dank textfaces in here yet. Use: *textface [lenny/tableflip/shrug]')
        else:
            if Type.lower() == 'lenny':
              await ctx.send('( ͡° ͜ʖ ͡°)')
            elif Type.lower() == 'tableflip':
              await ctx.send('(ノಠ益ಠ)ノ彡┻━┻')
            elif Type.lower() == 'shrug':
              await ctx.send('¯\_(ツ)_/¯')
            elif Type.lower() == 'bignose':
              await ctx.send('(͡ ͡° ͜ つ ͡͡°)')
            elif Type.lower() == 'iwant':
              await ctx.send('ლ(´ڡ`ლ)')
            elif Type.lower() == 'musicdude':
              await ctx.send('ヾ⌐*_*ノ♪')
            elif Type.lower() == 'wot':
              await ctx.send('ლ,ᔑ•ﺪ͟͠•ᔐ.ლ')
            elif Type.lower() == 'bomb':
              await ctx.send('(´・ω・)っ由')
            elif Type.lower() == 'orlly':
              await ctx.send("﴾͡๏̯͡๏﴿ O'RLY?")
            elif Type.lower() == 'money':
              await ctx.send('[̲̅$̲̅(̲̅ ͡° ͜ʖ ͡°̲̅)̲̅$̲̅]')
            elif Type.lower() == 'list':
              color = discord.Color(value=0x00ff00)
              em = discord.Embed(color=color, title='List of Textfaces')
              em.description = 'Choose from the following: lenny, tableflip, shrug, bignose, iwant, musicdude, wot, bomb, orlly, money. Type *textface [face].'
              em.set_footer(text="Don't you dare question my names for the textfaces.")
              await ctx.send(embed=em)
            else:
              await ctx.send('That is NOT one of the dank textfaces in here yet. Use *textface list to see a list of the textfaces.')
            
            
    @commands.command(aliases=['av'])
    async def avatar(self, ctx, user: discord.Member):
        """Returns a user's avatar url. Use *av [user], or just *av for your own."""
        if user is None:
            await ctx.send(ctx.message.author.avatar_url)                   
        else:
            await ctx.send(user.avatar_url)


    @commands.command()
    async def ping(self, ctx):
        """Premium ping pong giving you a websocket latency."""
        color = discord.Color(value=0xf9e236)
        e = discord.Embed(color=color, title='Pinging')
        e.description = 'Please wait... :ping_pong:'
        msg = await ctx.send(embed=e)
        em = discord.Embed(color=color, title='PoIIIng! Your supersonic latency is:')
        em.description = f"{self.bot.latency * 1000:.4f} ms"
        em.set_thumbnail(
        url="https://media.giphy.com/media/nE8wBpOIfKJKE/giphy.gif")
        await msg.edit(embed=em)
  
        
    @commands.command()
    async def botinv(self, ctx):
        """Allow my bot to join the hood. YOUR hood."""
        await ctx.send("Lemme join that hood -> https://discordapp.com/oauth2/authorize?client_id=508444259280617482&scope=bot&permissions=2146958591")                       

                       
    @commands.command(name='discord')
    async def _discord(self, ctx):
        """We have an awesome hood to join, join now!"""
        await ctx.send("Your turn to join the hood -> https://discord.gg/DTVRTBF")
        
        
    @commands.command(hidden=True)
    async def bug(self, ctx, *, msg:str):
        """Got a PROB? Tell us about it...  """
        lol = self.bot.get_channel(530573452197888030)
        color = discord.Color(value=random.randint(0x000000, 0xFFFFFF))
        em=discord.Embed(color=color, title="Bug reported!")
        em.description = f"Bug: {msg}"
        em.set_footer(text=f"Bug sent by {ctx.author}")                        
        await ctx.send("Thanks for reporting that bug! :bug: We will get back to you ASAP.")
        await lol.send(embed=em)                  
        await ctx.message.delete()


    @commands.command(hidden=True)
    async def suggest(self, ctx, *, msg:str):
        """Suggest changes and improvements for Zippy Bot#1064"""
        lol = self.bot.get_channel(530886981966757893)
        color = discord.Color(value=random.randint(0x000000, 0xFFFFFF))
        em=discord.Embed(color=color, title="New Suggestion")
        em.set_author(name=ctx.author, icon_url=ctx.message.author.avatar_url)
        em.description = f"Suggestion: {msg}"
        feedback_msg=await lol.send(embed=em)
        await feedback_msg.add_reaction('👍')
        await feedback_msg.add_reaction('👎')
        await asyncio.sleep(1)
        embed=discord.Embed(color=color, title="Success!")
        embed.description="Your suggestion has been sent"
        await ctx.send(embed=embed)
        await ctx.message.delete()


def setup(bot): 
    bot.add_cog(Utility(bot))
