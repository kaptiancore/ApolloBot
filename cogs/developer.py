import discord
import os
import json
import io
import sys
import textwrap
import traceback
import asyncio
from contextlib import redirect_stdout
from discord.ext import commands
from inspect import getsource
from .utils.paginator import HelpPaginator


class TabularData:
    def __init__(self):
        self._widths = []
        self._columns = []
        self._rows = []

    def set_columns(self, columns):
        self._columns = columns
        self._widths = [len(c) + 2 for c in columns]

    def add_row(self, row):
        rows = [str(r) for r in row]
        self._rows.append(rows)
        for index, element in enumerate(rows):
            width = len(element) + 2
            if width > self._widths[index]:
                self._widths[index] = width

    def add_rows(self, rows):
        for row in rows:
            self.add_row(row)

    def render(self):
        """Renders a table in rST format.
        Example:
        +-------+-----+
        | Name  | Age |
        +-------+-----+
        | Alice | 24  |
        |  Bob  | 19  |
        +-------+-----+
        """

        sep = '+'.join('-' * w for w in self._widths)
        sep = f'+{sep}+'

        to_draw = [sep]

        def get_entry(d):
            elem = '|'.join(f'{e:^{self._widths[i]}}' for i, e in enumerate(d))
            return f'|{elem}|'

        to_draw.append(get_entry(self._columns))
        to_draw.append(sep)

        for row in self._rows:
            to_draw.append(get_entry(row))

        to_draw.append(sep)
        return '\n'.join(to_draw)


class Plural:
    def __init__(self, **attr):
        iterator = attr.items()
        self.name, self.value = next(iter(iterator))

    def __str__(self):
        v = self.value
        if v == 0 or v > 1:
            return f'{v} {self.name}s'
        return f'{v} {self.name}'


class Developer(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._last_result = ''

    async def cog_command_error(self, ctx, error):
        if isinstance(error, commands.CheckFailure):
            e = discord.Embed(colour=discord.Colour.red())
            e.description = 'Sorry, this is a dev-only command. Thank You.'
            await ctx.send(embed=e)

    def cleanup_code(self, content):
        """Automatically removes code blocks from the code."""
        # remove ```py\n```
        if content.startswith('```') and content.endswith('```'):
            return '\n'.join(content.split('\n')[1:-1])
        return content.strip('` \n')

    def get_syntax_error(self, e):
        if e.text is None:
            return f'```py\n{e.__class__.__name__}: {e}\n```'
        return f'```py\n{e.text}{"^":>{e.offset}}\n{e.__class__.__name__}: {e}```'

    @commands.command()
    @commands.is_owner()
    async def restart(self, ctx):
        """Logs out and logs back into the bot
        [Owner only command]
        """
        await ctx.tick()
        os.execve(sys.executable, ['python3.6', 'main.py'], os.environ)

    @commands.command()
    @commands.is_owner()
    async def shutdown(self, ctx):
        """Shuts DOWN the bot. Cya!"""
        msg = await ctx.send(f"Shutting down... {self.bot.get_emoji(441385713091477504)}")
        await asyncio.sleep(1)
        await msg.edit(content="Goodbye! :wave:")
        await self.bot.logout()

    @commands.command()
    @commands.is_owner()
    async def changename(self, ctx, name=None):
        """Changes my name. Please make it good!"""
        if name is None:
            return await ctx.send("Hmm...my name cannot be blank!")
        else:
            await self.bot.user.edit(username=f'{name}')

    @commands.command()
    @commands.is_owner()
    async def load(self, ctx, cog):
        """Loads a selected cog"""
        msg = await ctx.send(f"Loading cog `{cog}`... :arrows_counterclockwise:")
        try:
            self.bot.load_extension(f"cogs.{cog}")
            await msg.edit(content="Loaded the cog! :white_check_mark:")
        except Exception as e:
            await msg.edit(content=f"An error occured. Details: \n{e}")

    @commands.command()
    @commands.is_owner()
    async def unload(self, ctx, cog):
        """Un-loads a selected cog"""
        msg = await ctx.send(f"Unloading cog `{cog}`... :arrows_counterclockwise:")
        try:
            self.bot.unload_extension(f"cogs.{cog}")
            await msg.edit(content="Unloaded the cog! :white_check_mark:")
        except Exception as e:
            await msg.edit(content=f"An error occured. Details: \n{e}")

    @commands.command()
    @commands.is_owner()
    async def reload(self, ctx, cog=None):
        """Reloads a selected cog"""
        msg = await ctx.send(f"Reloading cog `{cog}`... :arrows_counterclockwise:")
        try:
            self.bot.unload_extension(f"cogs.{cog}")
            self.bot.load_extension(f"cogs.{cog}")
            await msg.edit(content="Reloaded the cog! :white_check_mark:")
        except Exception as e:
            await msg.edit(content=f"An error occured. Details: \n{e}")

    @commands.is_owner()
    @commands.command()
    async def reload_all(self, ctx):
        for ext in self.bot.initial_extensions:
            self.bot.unload_extension(ext)
            self.bot.load_extension(ext)
        await ctx.tick()

    @commands.command()
    @commands.is_owner()
    async def source(self, ctx, command):
        """Get the source to any command"""
        cmd = self.bot.get_command(command)
        if cmd is None:
            return await ctx.send("Could not find that command.")
        await ctx.send(f"```py\n{getsource(cmd.callback)}```")

    @commands.command()
    @commands.is_owner()
    async def sudo(self, ctx, user: discord.Member, *, command: str):
        """do stuff as another user"""
        ctx.message.author = user
        ctx.message.content = f"{ctx.prefix}{command}"
        await self.bot.process_commands(ctx.message)

    @commands.command()
    @commands.is_owner()
    async def dm(self, ctx, user: discord.Member, *, msg: str):
        """Sends a DM message to the specified user."""
        embed = discord.Embed(title="Now sending message", description=msg, color=0x149900)
        embed.set_footer(text=user.name)
        await ctx.send(embed=embed)

        embed_2 = discord.Embed(title="You've received a message!", description=msg,color=0x149900)
        embed_2.set_footer(text=ctx.message.author.name)
        await user.send(embed=embed_2)

    @commands.command()
    @commands.is_owner()
    async def causeerror(self, ctx):
        """But why?!?!?"""
        [][0]

    @commands.command(name='eval')
    @commands.is_owner()
    async def _eval(self, ctx, *, body: str):
        """Evaluates a code"""

        env = {
            'bot': self.bot,
            'ctx': ctx,
            'channel': ctx.channel,
            'author': ctx.author,
            'guild': ctx.guild,
            'message': ctx.message,
            '_': self._last_result
        }

        env.update(globals())

        body = self.cleanup_code(body)
        stdout = io.StringIO()

        to_compile = f'async def func():\n{textwrap.indent(body, "  ")}'

        try:
            exec(to_compile, env)
        except Exception as e:
            return await ctx.send(f'```py\n{e.__class__.__name__}: {e}\n```')

        func = env['func']
        try:
            with redirect_stdout(stdout):
                ret = await func()
        except Exception as e:
            value = stdout.getvalue()
            await ctx.send(f'```py\n{value}{traceback.format_exc()}\n```')
        else:
            value = stdout.getvalue()
            try:
                await ctx.message.add_reaction('\u2705')
            except:
                pass

            if ret is None:
                if value:
                    await ctx.send(f'```py\n{value}\n```')
            else:
                self._last_result = ret
                await ctx.send(f'```py\n{value}{ret}\n```')

    @commands.command()
    @commands.is_owner()
    async def sql(self, ctx, *, query: str):
        """Run some SQL.

        [Owner only command]
        """
        # the imports are here because I imagine some people would want to use
        # this cog as a base for their other cog, and since this one is kinda
        # odd and unnecessary for most people, I will make it easy to remove
        # for those people.

        import time

        query = self.cleanup_code(query)

        is_multistatement = query.count(';') > 1
        if is_multistatement:
            # fetch does not support multiple statements
            strategy = ctx.db.execute
        else:
            strategy = ctx.db.fetch

        try:
            start = time.perf_counter()
            results = await strategy(query)
            dt = (time.perf_counter() - start) * 1000.0
        except Exception:
            return await ctx.send(f'```py\n{traceback.format_exc()}\n```')

        rows = len(results)
        if is_multistatement or rows == 0:
            return await ctx.send(f'`{dt:.2f}ms: {results}`')

        headers = list(results[0].keys())
        table = TabularData()
        table.set_columns(headers)
        table.add_rows(list(r.values()) for r in results)
        render = table.render()

        fmt = f'```\n{render}\n```\n*Returned {Plural(row=rows)} in {dt:.2f}ms*'
        if len(fmt) > 2000:
            fp = io.BytesIO(fmt.encode('utf-8'))
            await ctx.send('Too many results...', file=discord.File(fp, 'results.txt'))
        else:
            await ctx.send(fmt)

    @commands.command(name='help')
    async def _help(self, ctx, *, command: str = None):
        """Shows help about a command or the bot"""

        try:
            if command is None:
                p = await HelpPaginator.from_bot(ctx)
            else:
                entity = self.bot.get_cog(command) or self.bot.get_command(command.lower())

                if entity is None:
                    clean = command.replace('@', '@\u200b')
                    return await ctx.send(f'Command or category "{clean}" not found.')
                elif isinstance(entity, commands.Command):
                    p = await HelpPaginator.from_command(ctx, entity)
                else:
                    p = await HelpPaginator.from_cog(ctx, entity)

            await p.paginate()
        except Exception as e:
            await ctx.send(e)


def setup(bot):
    bot.add_cog(Developer(bot))
