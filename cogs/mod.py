import discord
import re
import shlex
import textwrap
import argparse
import datetime
from collections import Counter
from discord.ext import commands
from .utils.utils import Utils
from .utils import checks, time
import asyncio
from dateutil.relativedelta import relativedelta


def human_timedelta(dt, *, source=None):
    now = source or datetime.datetime.utcnow()
    if dt > now:
        delta = relativedelta(dt, now)
        suffix = ''
    else:
        delta = relativedelta(now, dt)
        suffix = ' ago'

    if delta.microseconds and delta.seconds:
        delta = delta + relativedelta(seconds=+1)

    attrs = ['years', 'months', 'days', 'hours', 'minutes', 'seconds']

    output = []
    for attr in attrs:
        elem = getattr(delta, attr)
        if not elem:
            continue

        if elem > 1:
            output.append(f'{elem} {attr}')
        else:
            output.append(f'{elem} {attr[:-1]}')

    if len(output) == 0:
        return 'now'
    elif len(output) == 1:
        return output[0] + suffix
    elif len(output) == 2:
        return f'{output[0]} and {output[1]}{suffix}'
    else:
        return f'{output[0]}, {output[1]} and {output[2]}{suffix}'

class Arguments(argparse.ArgumentParser):
    def error(self, message):
        raise RuntimeError(message)


class Moderator(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def cog_command_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            e = discord.Embed(colour=discord.Colour.red())
            e.description = str(error)
            await ctx.send(embed=e)

    @commands.group(name='warn', aliases=['warnings'], invoke_without_subcommand=True)
    @commands.has_permissions(manage_guild=True)
    async def _warnings(self, ctx, user: discord.Member, *, reason=None):
        """[Group] Manage Server Specific Warnings

        Invoke without a subcommand to warn someone with a reason (same as `warn add`)

        Usage: `warn [user] [reason: optional]`

        You must have `manage_guild` permissions
        """
        if ctx.invoked_subcommand is None:
            await ctx.invoke(self.bot.get_command('warn add'), user=user, reason=reason)

    @_warnings.command()
    async def add(self, ctx, user: discord.Member, reason=None):
        """Add a warning to a member - server specific

        Usage: `warn add [user] [reason: optional]`

        You must have `manage_guild` permissions
        """
        if not reason:
            db_reason = f'Warned by {str(ctx.author)} (no reason)'

        else:
            db_reason = f'Warned by {str(ctx.author)} Reason: {reason}'

        query = "INSERT INTO warnings (user_id, guild_id, reason, timestamp) VALUES ($1, $2, $3, $4);"
        await ctx.db.execute(query, user.id, ctx.guild.id, db_reason, ctx.message.created_at)

        await ctx.tick()

        await user.send(f"You have been warned by {str(ctx.author)} in {str(ctx.guild)} for: {reason or 'No Reason'}")

    @_warnings.command(aliases=['delete'])
    async def remove(self, ctx, warning_id: int):
        """Remove a warning from a member - server specific

        Usage: `warn add [id to remove]`

        ID can be found by using `warns [user]` or `warn show [user]` and is the number before the warning.

        You must have `manage_guild` permissions
        """
        query = "SELECT * FROM warnings WHERE guild_id = $1 AND id = $2"
        dump = await ctx.db.fetchrow(query, ctx.guild.id, warning_id)

        if not dump:
            raise commands.BadArgument('Warning ID not found for this guild.')

        query = "DELETE FROM warnings WHERE id = $1"
        await ctx.db.execute(query, warning_id)

        await ctx.tick()

    @_warnings.command(name='clear')
    async def _clear(self, ctx, user: discord.Member):
        """Clear warnings for a member - server specific

        Usage: `warn clear [user]`

        You must have `manage_guild` permissions
        """
        query = "DELETE FROM warnings WHERE guild_id = $1 and user_id = $2"
        await ctx.db.execute(query, ctx.guild.id, user.id)

        await ctx.tick()

    @_warnings.command(aliases=['list'])
    async def show(self, ctx, user: discord.Member=None):
        """Show warnings for a member - server specific

        Usage: `warn show [user: optional]`

        Specify no user to get warnings of yourself.

        You must have `manage_guild` permissions
        """
        if not user:
            user = ctx.author

        query = "SELECT id, reason, timestamp FROM warnings WHERE guild_id=$1 AND user_id=$2"
        dump = await ctx.db.fetch(query, ctx.guild.id, user.id)

        e = discord.Embed(colour=discord.Colour.blue())
        e.title = 'Previous infringements:'
        e.description = '\n\n'.join(f'{no}. {reas}, {ts.strftime("%Y-%m-%d %H:%M:%S")}'
                                    for (index, (no, reas, ts)) in enumerate(dump))
        e.set_footer(text=f'Total Warnings: {len(dump)}').timestamp = datetime.datetime.utcnow()

        await ctx.send(embed=e)

    @commands.command(hidden=True)
    async def warns(self, ctx, user: discord.Member=None):
        """Show warnings for a member - server specific

        Usage: `warn show [user: optional]`

        Specify no user to get warnings of yourself.

        You must have `manage_guild` permissions
        """
        await ctx.invoke(self.bot.get_command('warn show'), user=user)

    @commands.command()
    @commands.has_permissions(manage_guild=True)
    async def announce(self, ctx, channel: discord.TextChannel, *, msg: str):
        """announces a message to the designated channel"""
        await channel.send(msg)
        await ctx.tick()
        
    @commands.command(aliases=['newmembers'])
    @commands.guild_only()
    async def newusers(self, ctx, *, count=5):
        """Tells you the newest members of the server.
        This is useful to check if any suspicious members have
        joined.
        The count parameter can only be up to 25.
        """
        count = max(min(count, 25), 5)

        if not ctx.guild.chunked:
            await self.bot.request_offline_members(ctx.guild)

        members = sorted(ctx.guild.members, key=lambda m: m.joined_at, reverse=True)[:count]

        e = discord.Embed(title='New Members', colour=discord.Colour.green())

        for member in members:
            body = f'joined {time.human_timedelta(member.joined_at)}, created {time.human_timedelta(member.created_at)}'
            e.add_field(name=f'{member} (ID: {member.id})', value=body, inline=False)

        await ctx.send(embed=e)
        
    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(ban_members = True)
    async def lockdown(self, ctx, action: bool):
        """Prevents anyone from chatting in the current channel."""
        if action is True:
            msg = await ctx.send("Locking down the channel...")
            await ctx.channel.set_permissions(discord.utils.get(ctx.guild.roles, id=ctx.guild.id), send_messages=False)
            return await msg.edit(content="The channel has been successfully locked down. :lock: ")

        msg = await ctx.send("Unlocking the channel...")
        await ctx.channel.set_permissions(discord.utils.get(ctx.guild.roles, id=ctx.guild.id), send_messages=True)
        return await msg.edit(content="The channel has been successfully unlocked. :unlock: ")

    
    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    async def kick(self, ctx, user: discord.Member, *, reason=None):
        """Kicks a member into the world outside your server."""
        if user.id == ctx.author.id:
            return await ctx.send("I don't understand your logic. Kicking yourself?")
        if ctx.author.roles[-1].position < user.roles[-1].position:
            return await ctx.send(f"Sorry, but **{str(user)}**'s top role is higher than yours. No can do!")
        elif ctx.author.roles[-1].position == user.roles[-1].position:
            return await ctx.send(f"Sorry, but **{str(user)}**'s top role is equal to yours. No can do!")
        if not ctx.guild.me.guild_permissions.kick_members:
            return await ctx.send("Oops! I don't have enough permissions to use the boot.")
        try:
            await user.send(f"{self.bot.get_emoji(505723800068030464)} You have been kicked from **{ctx.guild.name}**.\n\n**Reason:** {reason}")
        except:
            pass
        try:
            await user.kick(reason=f"{str(ctx.author)}: {reason}")
            color = 0xf9e236
            em = discord.Embed(color=color, title='Kicked!')
            em.add_field(name='User', value=user.name)
            em.add_field(name='Kicked By', value=ctx.author.name)
            if reason is None:
                reason = 'No reason given.'
            else:
                reason = reason
            em.add_field(name='Reason', value=reason)
            em.set_thumbnail(url=user.avatar_url)
            em.set_image(url="https://cdn.weeb.sh/images/H16aQJFvb.gif")
            await ctx.send(embed=em)
        except discord.Forbidden:
            await ctx.send("Oops! I don't have enough permissions to use the boot.")

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def ban(self, ctx, user: discord.Member, *, reason=None):
        """Swings the mighty Ban Hammer on that bad boy."""
        if user.id == ctx.author.id:
            return await ctx.send("I don't understand your logic. Using the ban hammer on yourself?")
        if ctx.author.roles[-1].position < user.roles[-1].position:
            return await ctx.send(f"Sorry, but **{str(user)}**'s top role is higher than yours. No can do!")
        elif ctx.author.roles[-1].position == user.roles[-1].position:
            return await ctx.send(f"Sorry, but **{str(user)}**'s top role is equal to yours. No can do!")
        if not ctx.guild.me.guild_permissions.ban_members:
            return await ctx.send("Oops! I don't have enough permissions to swing this ban hammer.")
        try:
            await user.send(f"{self.bot.get_emoji(505723800068030464)} You have been banned from **{ctx.guild.name}**.\n\n**Reason:** {reason}")
        except:
            pass
        try:
            await user.ban(reason=reason, delete_message_days=0)
        except discord.Forbidden:
            return await ctx.send("Oops! I don't have enough permissions to swing this ban hammer.")
        color = 0xf9e236
        em = discord.Embed(color=color, title='Banned!')
        em.description = f'The ban hammer has fell down.'
        em.set_image(url="https://i.kym-cdn.com/photos/images/newsfeed/001/118/143/5ec.gif")
        em.add_field(name='User', value=user.name)
        em.add_field(name='Banned By', value=ctx.author.name)
        reason = reason if reason is not None else 'No reason given.'
        em.add_field(name='Reason', value=f"{str(ctx.author)}: {reason}")
        em.set_thumbnail(url=user.avatar_url)
        await ctx.send(embed=em)

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def unban(self, ctx, id, *, reason=None):
        match = re.match(r"[0-9]{16,18}", id) 
        if not match:
            return await ctx.send("That ain't a valid ID, am I right?")
        id = int(id)
        if not reason: reason = "No reason given."
        reason = f"{str(ctx.author)}: " + reason  
        bans = list(map(lambda e: e[1].id, await ctx.guild.bans()))
        if id not in bans:
            return await ctx.send("Uh-oh! It looks like one of two things just went down:\n1) The User ID is completely garbage and invalid.\n2) This user was not banned.")
        user = discord.Object(id)
        try:
            await ctx.guild.unban(user, reason=reason)
            em = discord.Embed(color=ctx.author.color, title="Member Unbanned!")
            em.description = "Party CONTINUED!"
            data = await self.bot.get_user_info(id)
            em.add_field(name="User", value=str(data))
            em.add_field(name="ID", value=id)
            em.add_field(name='Unbanned By', value=ctx.author.name)
            em.set_image(url="https://media.giphy.com/media/5wWf7GQ50RTXOxjcX5K/giphy.gif")
            em.add_field(name='Reason', value=reason)
            em.set_thumbnail(url=data.avatar_url)
            await ctx.send(embed=em)
        except discord.Forbidden:
            return await ctx.send("Will you look at that? I don't have the **Ban Members** permission for this command.")

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def mute(self, ctx, user: discord.Member, time_to_mute: int):
        '''Forces someone to shut up. Usage: *mute [user] [time in mins]'''
        try:
            msg = await ctx.send(f"I'm muting up the user right now... :zipper_mouth:")
            for chan in ctx.guild.channels:
                await chan.set_permissions(user, send_messages=False, add_reactions=False)
            try:
                await msg.delete()
                await ctx.send(f"Hey, {user.mention}, mind keeping your mouth shut? :zipper_mouth: ")
            except:
                await msg.edit(content=f"Hey, {user.mention}, mind keeping your mouth shut? :zipper_mouth: ")
        except discord.Forbidden:
            return await ctx.send("I could not mute the user. Make sure I have the manage channels permission.")

        await asyncio.sleep(time_to_mute*60)
        for chan in ctx.guild.channels:
            await chan.set_permissions(user, send_messages=None, add_reactions=None)

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def unmute(self, ctx, user: discord.Member):
        '''Allows someone to un-shut up. Usage: *unmute [user]'''
        try:
            msg = await ctx.send(f"Alright, I'll unmute the user. Hang on... {self.bot.get_emoji(485594540192038925)}")
            for chan in ctx.guild.channels:
                await chan.set_permissions(user, send_messages=None, add_reactions=None)
            try:
                await msg.delete()
                await ctx.send(f"Alright then, {user.mention}, I spared your life. Open your mouth and continue the party! {self.bot.get_emoji(527225741855817738)}")
            except:
                await msg.edit(content=f"Alright then, {user.mention}, I spared your life. Open your mouth and continue the party! {self.bot.get_emoji(527225741855817738)}")
        except discord.Forbidden:
            await ctx.send("Couldn't unmute the user. Uh-oh...")
    
    @commands.command(aliases=['clean', 'purge'])
    @commands.guild_only()
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(manage_messages=True)
    async def clear(self, ctx, num: int, from_user: discord.Member = None):
        """Deletes a # of msgs. *clear [# of msgs]."""
        try:
            if not from_user:
                await ctx.channel.purge(limit=num+1, bulk=True)
            else:
                await ctx.channel.purge(limit=num+1, bulk=True, check=lambda x: x.author.id == from_user.id)
            msg = await ctx.send("Cleared successfully :white_check_mark:", delete_after=3)
        except discord.Forbidden:
            await ctx.send("Clear unsuccessful. The bot does not have Manage Msgs permission.")
            
    @commands.command(aliases=['arole'])
    @commands.guild_only()
    @commands.has_permissions(manage_roles = True)
    @commands.bot_has_permissions(manage_roles=True)
    async def addrole(self, ctx, user: discord.Member, role: discord.Role):
        """Adds a role to a user."""
        if ctx.author.roles[-1].position < role.position:
            return await ctx.send(f"Sorry, but the position of **{str(role)}** is higher than yours. No can do!")
        elif ctx.author.roles[-1].position == role.position:
            return await ctx.send(f"Sorry, but the position of **{str(role)}** is equal to yours. No can do!")

        if role.name in [x.name for x in user.roles]:
            return await ctx.send(f"Looks like **{str(user)}** already has the role **{role}**.")
        try:
            await user.add_roles(role)
            return await ctx.send(f"Success! **{str(user)}** has been given the role **{role}**.")
        except discord.Forbidden:
            return await ctx.send("Bot does not have enough permissions to give roles.")
        
    @commands.command(aliases=['rrole'])
    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def removerole(self, ctx, user: discord.Member, role: discord.Role):
        """Removes a role from a user."""
        if ctx.author.roles[-1].position < role.position:
            return await ctx.send(f"Sorry, but the position of **{str(role)}** is higher than yours. No can do!")
        elif ctx.author.roles[-1].position == role.position:
            return await ctx.send(f"Sorry, but the position of **{str(role)}** is equal to yours. No can do!")
        if role.name not in [x.name for x in user.roles]:
            return await ctx.send(f"Looks like **{str(user)}** never had the role **{role}**.")
        try:
            await user.remove_roles(role)
            return await ctx.send(f"Success! **{str(user)}** has been removed from the role **{role}**.")
        except discord.Forbidden:
            return await ctx.send("Bot does not have enough permissions to remove role.")

    @commands.command()
    @commands.guild_only()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    async def hackban(self, ctx, id : int, *, reason=None):
        if ctx.guild.get_member(id):
            return await ctx.send("Senpai, hackban is to ban people **not in the server.** If you wanna ban someone in the server, run `*ban @user`.")
        if not ctx.guild.me.guild_permissions.kick_members:
            return await ctx.send("Oops! I don't have enough permissions to use the boot.")
        try:
            id = int(id)
        except ValueError:
            return await ctx.send("Did you enter a valid user ID?")
        lol = discord.Object(id)
        if not lol:
            return await ctx.send("Invalid ID provided.")
        try:
            await ctx.guild.ban(lol, reason=reason, delete_message_days=0)
        except discord.Forbidden:
            await ctx.send("Oops! I don't have enough permissions to swing this ban hammer.")
        data = await self.bot.get_user_info(id)
        color = 0xf9e236
        em = discord.Embed(color=color, title='Banned!')
        em.add_field(name="User", value=str(data))
        em.add_field(name="ID", value=id)
        em.add_field(name='Banned By', value=ctx.author.name)
        em.set_image(url="https://i.kym-cdn.com/photos/images/newsfeed/001/118/143/5ec.gif")
        reason = reason if reason is not None else 'No reason given.'
        em.add_field(name='Reason', value=reason)
        em.set_thumbnail(url=data.avatar_url)
        await ctx.send(embed=em)


def setup(bot): 
    bot.add_cog(Moderator(bot))
