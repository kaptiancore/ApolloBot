# Apollo Bot
![logo](https://cdn.discordapp.com/avatars/508444259280617482/3aee63cd0ff0de7519627799fa6bf44a.webp?size=100x20) ![size](https://img.shields.io/github/repo-size/Cerberus-75/ApolloBot.svg) ![commit](https://img.shields.io/github/last-commit/Cerberus-75/ApolloBot.svg) ![version](https://img.shields.io/badge/python-3.6-blue.svg) ![license](https://img.shields.io/github/license/Cerberus-75/ApolloBot.svg)![supportserver](https://img.shields.io/discord/529590413078822913.svg?style=plastic)

"Just a bot we made" it is a collaboration between Nonna, Kaptian Core and MathsMan 
aimed to learn new things and have good working bot that we can take credit for.

## Getting Started

The bot will not be aloud to be used until it is the right time and will now only work for us.
This bot may be open for setting up and using in the future but for now it will be hosted by us 
if there is anything you want us to add to Apollo join the [support server](https://discord.gg/DTVRTBF) and suggest it and if it's a good idea and would be worth while adding it, it will be implemented into the bot when it gets to the top positing on the todo list.

## What was this bot coded in?

This was coded in Discord.py rewrite.

## Support Server & Inviting The Bot To Your Server

[Invite Apollo Bot To Your Server](https://discordapp.com/oauth2/authorize?client_id=508444259280617482&scope=bot&permissions=2146958591)

[Support Server Link](https://discord.gg/DTVRTBF)

## Authors

* **Kaptian Core** - *Developer* - [KaptianCore](https://github.com/kaptiancore)
* **Nonna** - *Developer* - [Nonna](https://github.com/Cerberus-75)
* **MathsMan** - *Developer* - [MathsMan](https://github.com/mathsman5133)

See also the list of [contributors](https://github.com/Cerberus-75/bot/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
